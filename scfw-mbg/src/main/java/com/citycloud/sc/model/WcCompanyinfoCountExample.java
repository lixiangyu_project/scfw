package com.citycloud.sc.model;

import java.util.ArrayList;
import java.util.List;

public class WcCompanyinfoCountExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WcCompanyinfoCountExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andYearIsNull() {
            addCriterion("year is null");
            return (Criteria) this;
        }

        public Criteria andYearIsNotNull() {
            addCriterion("year is not null");
            return (Criteria) this;
        }

        public Criteria andYearEqualTo(String value) {
            addCriterion("year =", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotEqualTo(String value) {
            addCriterion("year <>", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearGreaterThan(String value) {
            addCriterion("year >", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearGreaterThanOrEqualTo(String value) {
            addCriterion("year >=", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearLessThan(String value) {
            addCriterion("year <", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearLessThanOrEqualTo(String value) {
            addCriterion("year <=", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearLike(String value) {
            addCriterion("year like", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotLike(String value) {
            addCriterion("year not like", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearIn(List<String> values) {
            addCriterion("year in", values, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotIn(List<String> values) {
            addCriterion("year not in", values, "year");
            return (Criteria) this;
        }

        public Criteria andYearBetween(String value1, String value2) {
            addCriterion("year between", value1, value2, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotBetween(String value1, String value2) {
            addCriterion("year not between", value1, value2, "year");
            return (Criteria) this;
        }

        public Criteria andStreetIsNull() {
            addCriterion("street is null");
            return (Criteria) this;
        }

        public Criteria andStreetIsNotNull() {
            addCriterion("street is not null");
            return (Criteria) this;
        }

        public Criteria andStreetEqualTo(String value) {
            addCriterion("street =", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetNotEqualTo(String value) {
            addCriterion("street <>", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetGreaterThan(String value) {
            addCriterion("street >", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetGreaterThanOrEqualTo(String value) {
            addCriterion("street >=", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetLessThan(String value) {
            addCriterion("street <", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetLessThanOrEqualTo(String value) {
            addCriterion("street <=", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetLike(String value) {
            addCriterion("street like", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetNotLike(String value) {
            addCriterion("street not like", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetIn(List<String> values) {
            addCriterion("street in", values, "street");
            return (Criteria) this;
        }

        public Criteria andStreetNotIn(List<String> values) {
            addCriterion("street not in", values, "street");
            return (Criteria) this;
        }

        public Criteria andStreetBetween(String value1, String value2) {
            addCriterion("street between", value1, value2, "street");
            return (Criteria) this;
        }

        public Criteria andStreetNotBetween(String value1, String value2) {
            addCriterion("street not between", value1, value2, "street");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andCreditCodeIsNull() {
            addCriterion("credit_code is null");
            return (Criteria) this;
        }

        public Criteria andCreditCodeIsNotNull() {
            addCriterion("credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andCreditCodeEqualTo(String value) {
            addCriterion("credit_code =", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeNotEqualTo(String value) {
            addCriterion("credit_code <>", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeGreaterThan(String value) {
            addCriterion("credit_code >", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("credit_code >=", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeLessThan(String value) {
            addCriterion("credit_code <", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("credit_code <=", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeLike(String value) {
            addCriterion("credit_code like", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeNotLike(String value) {
            addCriterion("credit_code not like", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeIn(List<String> values) {
            addCriterion("credit_code in", values, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeNotIn(List<String> values) {
            addCriterion("credit_code not in", values, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeBetween(String value1, String value2) {
            addCriterion("credit_code between", value1, value2, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeNotBetween(String value1, String value2) {
            addCriterion("credit_code not between", value1, value2, "creditCode");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueIsNull() {
            addCriterion("increase_value is null");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueIsNotNull() {
            addCriterion("increase_value is not null");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueEqualTo(String value) {
            addCriterion("increase_value =", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueNotEqualTo(String value) {
            addCriterion("increase_value <>", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueGreaterThan(String value) {
            addCriterion("increase_value >", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueGreaterThanOrEqualTo(String value) {
            addCriterion("increase_value >=", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueLessThan(String value) {
            addCriterion("increase_value <", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueLessThanOrEqualTo(String value) {
            addCriterion("increase_value <=", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueLike(String value) {
            addCriterion("increase_value like", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueNotLike(String value) {
            addCriterion("increase_value not like", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueIn(List<String> values) {
            addCriterion("increase_value in", values, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueNotIn(List<String> values) {
            addCriterion("increase_value not in", values, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueBetween(String value1, String value2) {
            addCriterion("increase_value between", value1, value2, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueNotBetween(String value1, String value2) {
            addCriterion("increase_value not between", value1, value2, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andPeriodIsNull() {
            addCriterion("period is null");
            return (Criteria) this;
        }

        public Criteria andPeriodIsNotNull() {
            addCriterion("period is not null");
            return (Criteria) this;
        }

        public Criteria andPeriodEqualTo(String value) {
            addCriterion("period =", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotEqualTo(String value) {
            addCriterion("period <>", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodGreaterThan(String value) {
            addCriterion("period >", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodGreaterThanOrEqualTo(String value) {
            addCriterion("period >=", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLessThan(String value) {
            addCriterion("period <", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLessThanOrEqualTo(String value) {
            addCriterion("period <=", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLike(String value) {
            addCriterion("period like", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotLike(String value) {
            addCriterion("period not like", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodIn(List<String> values) {
            addCriterion("period in", values, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotIn(List<String> values) {
            addCriterion("period not in", values, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodBetween(String value1, String value2) {
            addCriterion("period between", value1, value2, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotBetween(String value1, String value2) {
            addCriterion("period not between", value1, value2, "period");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedIsNull() {
            addCriterion("increase_speed is null");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedIsNotNull() {
            addCriterion("increase_speed is not null");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedEqualTo(String value) {
            addCriterion("increase_speed =", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedNotEqualTo(String value) {
            addCriterion("increase_speed <>", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedGreaterThan(String value) {
            addCriterion("increase_speed >", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedGreaterThanOrEqualTo(String value) {
            addCriterion("increase_speed >=", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedLessThan(String value) {
            addCriterion("increase_speed <", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedLessThanOrEqualTo(String value) {
            addCriterion("increase_speed <=", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedLike(String value) {
            addCriterion("increase_speed like", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedNotLike(String value) {
            addCriterion("increase_speed not like", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedIn(List<String> values) {
            addCriterion("increase_speed in", values, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedNotIn(List<String> values) {
            addCriterion("increase_speed not in", values, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedBetween(String value1, String value2) {
            addCriterion("increase_speed between", value1, value2, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedNotBetween(String value1, String value2) {
            addCriterion("increase_speed not between", value1, value2, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andSupportProjectnumIsNull() {
            addCriterion("support_projectNum is null");
            return (Criteria) this;
        }

        public Criteria andSupportProjectnumIsNotNull() {
            addCriterion("support_projectNum is not null");
            return (Criteria) this;
        }

        public Criteria andSupportProjectnumEqualTo(Integer value) {
            addCriterion("support_projectNum =", value, "supportProjectnum");
            return (Criteria) this;
        }

        public Criteria andSupportProjectnumNotEqualTo(Integer value) {
            addCriterion("support_projectNum <>", value, "supportProjectnum");
            return (Criteria) this;
        }

        public Criteria andSupportProjectnumGreaterThan(Integer value) {
            addCriterion("support_projectNum >", value, "supportProjectnum");
            return (Criteria) this;
        }

        public Criteria andSupportProjectnumGreaterThanOrEqualTo(Integer value) {
            addCriterion("support_projectNum >=", value, "supportProjectnum");
            return (Criteria) this;
        }

        public Criteria andSupportProjectnumLessThan(Integer value) {
            addCriterion("support_projectNum <", value, "supportProjectnum");
            return (Criteria) this;
        }

        public Criteria andSupportProjectnumLessThanOrEqualTo(Integer value) {
            addCriterion("support_projectNum <=", value, "supportProjectnum");
            return (Criteria) this;
        }

        public Criteria andSupportProjectnumIn(List<Integer> values) {
            addCriterion("support_projectNum in", values, "supportProjectnum");
            return (Criteria) this;
        }

        public Criteria andSupportProjectnumNotIn(List<Integer> values) {
            addCriterion("support_projectNum not in", values, "supportProjectnum");
            return (Criteria) this;
        }

        public Criteria andSupportProjectnumBetween(Integer value1, Integer value2) {
            addCriterion("support_projectNum between", value1, value2, "supportProjectnum");
            return (Criteria) this;
        }

        public Criteria andSupportProjectnumNotBetween(Integer value1, Integer value2) {
            addCriterion("support_projectNum not between", value1, value2, "supportProjectnum");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumIsNull() {
            addCriterion("support_monenyNum is null");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumIsNotNull() {
            addCriterion("support_monenyNum is not null");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumEqualTo(String value) {
            addCriterion("support_monenyNum =", value, "supportMonenynum");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumNotEqualTo(String value) {
            addCriterion("support_monenyNum <>", value, "supportMonenynum");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumGreaterThan(String value) {
            addCriterion("support_monenyNum >", value, "supportMonenynum");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumGreaterThanOrEqualTo(String value) {
            addCriterion("support_monenyNum >=", value, "supportMonenynum");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumLessThan(String value) {
            addCriterion("support_monenyNum <", value, "supportMonenynum");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumLessThanOrEqualTo(String value) {
            addCriterion("support_monenyNum <=", value, "supportMonenynum");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumLike(String value) {
            addCriterion("support_monenyNum like", value, "supportMonenynum");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumNotLike(String value) {
            addCriterion("support_monenyNum not like", value, "supportMonenynum");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumIn(List<String> values) {
            addCriterion("support_monenyNum in", values, "supportMonenynum");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumNotIn(List<String> values) {
            addCriterion("support_monenyNum not in", values, "supportMonenynum");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumBetween(String value1, String value2) {
            addCriterion("support_monenyNum between", value1, value2, "supportMonenynum");
            return (Criteria) this;
        }

        public Criteria andSupportMonenynumNotBetween(String value1, String value2) {
            addCriterion("support_monenyNum not between", value1, value2, "supportMonenynum");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeIsNull() {
            addCriterion("tax_income is null");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeIsNotNull() {
            addCriterion("tax_income is not null");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeEqualTo(String value) {
            addCriterion("tax_income =", value, "taxIncome");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeNotEqualTo(String value) {
            addCriterion("tax_income <>", value, "taxIncome");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeGreaterThan(String value) {
            addCriterion("tax_income >", value, "taxIncome");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeGreaterThanOrEqualTo(String value) {
            addCriterion("tax_income >=", value, "taxIncome");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeLessThan(String value) {
            addCriterion("tax_income <", value, "taxIncome");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeLessThanOrEqualTo(String value) {
            addCriterion("tax_income <=", value, "taxIncome");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeLike(String value) {
            addCriterion("tax_income like", value, "taxIncome");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeNotLike(String value) {
            addCriterion("tax_income not like", value, "taxIncome");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeIn(List<String> values) {
            addCriterion("tax_income in", values, "taxIncome");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeNotIn(List<String> values) {
            addCriterion("tax_income not in", values, "taxIncome");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeBetween(String value1, String value2) {
            addCriterion("tax_income between", value1, value2, "taxIncome");
            return (Criteria) this;
        }

        public Criteria andTaxIncomeNotBetween(String value1, String value2) {
            addCriterion("tax_income not between", value1, value2, "taxIncome");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}