package com.citycloud.sc.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;

public class WcStreetinfoCount implements Serializable {
    @ApiModelProperty(value = "主键ID")
    private Integer id;

    @ApiModelProperty(value = "年度")
    private String year;

    @ApiModelProperty(value = "类型")
    private String type;

    @ApiModelProperty(value = "属地街道")
    private String street;

    @ApiModelProperty(value = "增加值(万元)")
    private BigDecimal increaseValue;

    @ApiModelProperty(value = "去年同期")
    private BigDecimal period;

    @ApiModelProperty(value = "增速(%)")
    private BigDecimal increaseSpeed;

    @ApiModelProperty(value = "企业数量")
    private Integer companyNumber;

    @ApiModelProperty(value = "年度税收收入（万元）")
    private BigDecimal taxIncome;

    @ApiModelProperty(value = "年度地方财政收入(万元)")
    private BigDecimal financeIncome;

    @ApiModelProperty(value = "扶持企业数量")
    private Integer supportCompanynum;

    @ApiModelProperty(value = "扶持项目数量")
    private Integer supportProjectnum;

    @ApiModelProperty(value = "扶持金额（万元）")
    private BigDecimal supportMonenynum;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public BigDecimal getIncreaseValue() {
        return increaseValue;
    }

    public void setIncreaseValue(BigDecimal increaseValue) {
        this.increaseValue = increaseValue;
    }

    public BigDecimal getPeriod() {
        return period;
    }

    public void setPeriod(BigDecimal period) {
        this.period = period;
    }

    public BigDecimal getIncreaseSpeed() {
        return increaseSpeed;
    }

    public void setIncreaseSpeed(BigDecimal increaseSpeed) {
        this.increaseSpeed = increaseSpeed;
    }

    public Integer getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyNumber(Integer companyNumber) {
        this.companyNumber = companyNumber;
    }

    public BigDecimal getTaxIncome() {
        return taxIncome;
    }

    public void setTaxIncome(BigDecimal taxIncome) {
        this.taxIncome = taxIncome;
    }

    public BigDecimal getFinanceIncome() {
        return financeIncome;
    }

    public void setFinanceIncome(BigDecimal financeIncome) {
        this.financeIncome = financeIncome;
    }

    public Integer getSupportCompanynum() {
        return supportCompanynum;
    }

    public void setSupportCompanynum(Integer supportCompanynum) {
        this.supportCompanynum = supportCompanynum;
    }

    public Integer getSupportProjectnum() {
        return supportProjectnum;
    }

    public void setSupportProjectnum(Integer supportProjectnum) {
        this.supportProjectnum = supportProjectnum;
    }

    public BigDecimal getSupportMonenynum() {
        return supportMonenynum;
    }

    public void setSupportMonenynum(BigDecimal supportMonenynum) {
        this.supportMonenynum = supportMonenynum;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", year=").append(year);
        sb.append(", type=").append(type);
        sb.append(", street=").append(street);
        sb.append(", increaseValue=").append(increaseValue);
        sb.append(", period=").append(period);
        sb.append(", increaseSpeed=").append(increaseSpeed);
        sb.append(", companyNumber=").append(companyNumber);
        sb.append(", taxIncome=").append(taxIncome);
        sb.append(", financeIncome=").append(financeIncome);
        sb.append(", supportCompanynum=").append(supportCompanynum);
        sb.append(", supportProjectnum=").append(supportProjectnum);
        sb.append(", supportMonenynum=").append(supportMonenynum);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}