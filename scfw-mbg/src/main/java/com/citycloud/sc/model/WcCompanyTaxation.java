package com.citycloud.sc.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

public class WcCompanyTaxation implements Serializable {
    @ApiModelProperty(value = "主键ID")
    private Integer id;

    @ApiModelProperty(value = "年度")
    private String year;

    @ApiModelProperty(value = "属地街道")
    private String street;

    @ApiModelProperty(value = "企业名称")
    private String name;

    @ApiModelProperty(value = "统一信用代码")
    private String creditCode;

    @ApiModelProperty(value = "所属产业")
    private String property;

    @ApiModelProperty(value = "税收收入本年（万元）")
    private String taxIncome;

    @ApiModelProperty(value = "去年同期（万元）")
    private String period;

    @ApiModelProperty(value = "地方财政收入本年(万元)")
    private String financeIncome;

    @ApiModelProperty(value = "导入时间")
    private Date importTime;

    @ApiModelProperty(value = "导入人")
    private String importer;

    @ApiModelProperty(value = "上传文件")
    private String file;

    @ApiModelProperty(value = "文件ID")
    private String fileid;

    @ApiModelProperty(value = "文件名称")
    private String filename;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreditCode() {
        return creditCode;
    }

    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getTaxIncome() {
        return taxIncome;
    }

    public void setTaxIncome(String taxIncome) {
        this.taxIncome = taxIncome;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getFinanceIncome() {
        return financeIncome;
    }

    public void setFinanceIncome(String financeIncome) {
        this.financeIncome = financeIncome;
    }

    public Date getImportTime() {
        return importTime;
    }

    public void setImportTime(Date importTime) {
        this.importTime = importTime;
    }

    public String getImporter() {
        return importer;
    }

    public void setImporter(String importer) {
        this.importer = importer;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", year=").append(year);
        sb.append(", street=").append(street);
        sb.append(", name=").append(name);
        sb.append(", creditCode=").append(creditCode);
        sb.append(", property=").append(property);
        sb.append(", taxIncome=").append(taxIncome);
        sb.append(", period=").append(period);
        sb.append(", financeIncome=").append(financeIncome);
        sb.append(", importTime=").append(importTime);
        sb.append(", importer=").append(importer);
        sb.append(", file=").append(file);
        sb.append(", fileid=").append(fileid);
        sb.append(", filename=").append(filename);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}