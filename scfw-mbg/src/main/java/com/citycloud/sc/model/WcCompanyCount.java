package com.citycloud.sc.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

public class WcCompanyCount implements Serializable {
    @ApiModelProperty(value = "主键ID")
    private Integer id;

    @ApiModelProperty(value = "年度")
    private String year;

    @ApiModelProperty(value = "属地街道")
    private String street;

    @ApiModelProperty(value = "企业名称")
    private String name;

    @ApiModelProperty(value = "行业代码")
    private String industryCode;

    @ApiModelProperty(value = "统一信用代码")
    private String creditCode;

    @ApiModelProperty(value = "单位类型")
    private String unitType;

    @ApiModelProperty(value = "营业收入")
    private String income;

    @ApiModelProperty(value = "营业收入增速(%)")
    private String incomeSpeed;

    @ApiModelProperty(value = "营业利润")
    private String profit;

    @ApiModelProperty(value = "营业利润增速(%)")
    private String profitSpeed;

    @ApiModelProperty(value = "平均人数")
    private String averageNumber;

    @ApiModelProperty(value = "去年同期")
    private String period;

    @ApiModelProperty(value = "人数增速(%)")
    private String numberSpeed;

    @ApiModelProperty(value = "增加值(万元)")
    private String increaseValue;

    @ApiModelProperty(value = "增速(%)")
    private String increaseSpeed;

    @ApiModelProperty(value = "上传文件")
    private String file;

    @ApiModelProperty(value = "文件ID")
    private String fileid;

    @ApiModelProperty(value = "文件名称")
    private String filename;

    @ApiModelProperty(value = "导入时间")
    private Date importTime;

    @ApiModelProperty(value = "导入人")
    private String importer;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }

    public String getCreditCode() {
        return creditCode;
    }

    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getIncomeSpeed() {
        return incomeSpeed;
    }

    public void setIncomeSpeed(String incomeSpeed) {
        this.incomeSpeed = incomeSpeed;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getProfitSpeed() {
        return profitSpeed;
    }

    public void setProfitSpeed(String profitSpeed) {
        this.profitSpeed = profitSpeed;
    }

    public String getAverageNumber() {
        return averageNumber;
    }

    public void setAverageNumber(String averageNumber) {
        this.averageNumber = averageNumber;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getNumberSpeed() {
        return numberSpeed;
    }

    public void setNumberSpeed(String numberSpeed) {
        this.numberSpeed = numberSpeed;
    }

    public String getIncreaseValue() {
        return increaseValue;
    }

    public void setIncreaseValue(String increaseValue) {
        this.increaseValue = increaseValue;
    }

    public String getIncreaseSpeed() {
        return increaseSpeed;
    }

    public void setIncreaseSpeed(String increaseSpeed) {
        this.increaseSpeed = increaseSpeed;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Date getImportTime() {
        return importTime;
    }

    public void setImportTime(Date importTime) {
        this.importTime = importTime;
    }

    public String getImporter() {
        return importer;
    }

    public void setImporter(String importer) {
        this.importer = importer;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", year=").append(year);
        sb.append(", street=").append(street);
        sb.append(", name=").append(name);
        sb.append(", industryCode=").append(industryCode);
        sb.append(", creditCode=").append(creditCode);
        sb.append(", unitType=").append(unitType);
        sb.append(", income=").append(income);
        sb.append(", incomeSpeed=").append(incomeSpeed);
        sb.append(", profit=").append(profit);
        sb.append(", profitSpeed=").append(profitSpeed);
        sb.append(", averageNumber=").append(averageNumber);
        sb.append(", period=").append(period);
        sb.append(", numberSpeed=").append(numberSpeed);
        sb.append(", increaseValue=").append(increaseValue);
        sb.append(", increaseSpeed=").append(increaseSpeed);
        sb.append(", file=").append(file);
        sb.append(", fileid=").append(fileid);
        sb.append(", filename=").append(filename);
        sb.append(", importTime=").append(importTime);
        sb.append(", importer=").append(importer);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}