package com.citycloud.sc.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WcCompanyCountExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WcCompanyCountExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andYearIsNull() {
            addCriterion("year is null");
            return (Criteria) this;
        }

        public Criteria andYearIsNotNull() {
            addCriterion("year is not null");
            return (Criteria) this;
        }

        public Criteria andYearEqualTo(String value) {
            addCriterion("year =", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotEqualTo(String value) {
            addCriterion("year <>", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearGreaterThan(String value) {
            addCriterion("year >", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearGreaterThanOrEqualTo(String value) {
            addCriterion("year >=", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearLessThan(String value) {
            addCriterion("year <", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearLessThanOrEqualTo(String value) {
            addCriterion("year <=", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearLike(String value) {
            addCriterion("year like", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotLike(String value) {
            addCriterion("year not like", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearIn(List<String> values) {
            addCriterion("year in", values, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotIn(List<String> values) {
            addCriterion("year not in", values, "year");
            return (Criteria) this;
        }

        public Criteria andYearBetween(String value1, String value2) {
            addCriterion("year between", value1, value2, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotBetween(String value1, String value2) {
            addCriterion("year not between", value1, value2, "year");
            return (Criteria) this;
        }

        public Criteria andStreetIsNull() {
            addCriterion("street is null");
            return (Criteria) this;
        }

        public Criteria andStreetIsNotNull() {
            addCriterion("street is not null");
            return (Criteria) this;
        }

        public Criteria andStreetEqualTo(String value) {
            addCriterion("street =", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetNotEqualTo(String value) {
            addCriterion("street <>", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetGreaterThan(String value) {
            addCriterion("street >", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetGreaterThanOrEqualTo(String value) {
            addCriterion("street >=", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetLessThan(String value) {
            addCriterion("street <", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetLessThanOrEqualTo(String value) {
            addCriterion("street <=", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetLike(String value) {
            addCriterion("street like", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetNotLike(String value) {
            addCriterion("street not like", value, "street");
            return (Criteria) this;
        }

        public Criteria andStreetIn(List<String> values) {
            addCriterion("street in", values, "street");
            return (Criteria) this;
        }

        public Criteria andStreetNotIn(List<String> values) {
            addCriterion("street not in", values, "street");
            return (Criteria) this;
        }

        public Criteria andStreetBetween(String value1, String value2) {
            addCriterion("street between", value1, value2, "street");
            return (Criteria) this;
        }

        public Criteria andStreetNotBetween(String value1, String value2) {
            addCriterion("street not between", value1, value2, "street");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeIsNull() {
            addCriterion("industry_code is null");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeIsNotNull() {
            addCriterion("industry_code is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeEqualTo(String value) {
            addCriterion("industry_code =", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeNotEqualTo(String value) {
            addCriterion("industry_code <>", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeGreaterThan(String value) {
            addCriterion("industry_code >", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeGreaterThanOrEqualTo(String value) {
            addCriterion("industry_code >=", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeLessThan(String value) {
            addCriterion("industry_code <", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeLessThanOrEqualTo(String value) {
            addCriterion("industry_code <=", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeLike(String value) {
            addCriterion("industry_code like", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeNotLike(String value) {
            addCriterion("industry_code not like", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeIn(List<String> values) {
            addCriterion("industry_code in", values, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeNotIn(List<String> values) {
            addCriterion("industry_code not in", values, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeBetween(String value1, String value2) {
            addCriterion("industry_code between", value1, value2, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeNotBetween(String value1, String value2) {
            addCriterion("industry_code not between", value1, value2, "industryCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeIsNull() {
            addCriterion("credit_code is null");
            return (Criteria) this;
        }

        public Criteria andCreditCodeIsNotNull() {
            addCriterion("credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andCreditCodeEqualTo(String value) {
            addCriterion("credit_code =", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeNotEqualTo(String value) {
            addCriterion("credit_code <>", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeGreaterThan(String value) {
            addCriterion("credit_code >", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("credit_code >=", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeLessThan(String value) {
            addCriterion("credit_code <", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("credit_code <=", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeLike(String value) {
            addCriterion("credit_code like", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeNotLike(String value) {
            addCriterion("credit_code not like", value, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeIn(List<String> values) {
            addCriterion("credit_code in", values, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeNotIn(List<String> values) {
            addCriterion("credit_code not in", values, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeBetween(String value1, String value2) {
            addCriterion("credit_code between", value1, value2, "creditCode");
            return (Criteria) this;
        }

        public Criteria andCreditCodeNotBetween(String value1, String value2) {
            addCriterion("credit_code not between", value1, value2, "creditCode");
            return (Criteria) this;
        }

        public Criteria andUnitTypeIsNull() {
            addCriterion("unit_type is null");
            return (Criteria) this;
        }

        public Criteria andUnitTypeIsNotNull() {
            addCriterion("unit_type is not null");
            return (Criteria) this;
        }

        public Criteria andUnitTypeEqualTo(String value) {
            addCriterion("unit_type =", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotEqualTo(String value) {
            addCriterion("unit_type <>", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeGreaterThan(String value) {
            addCriterion("unit_type >", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeGreaterThanOrEqualTo(String value) {
            addCriterion("unit_type >=", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeLessThan(String value) {
            addCriterion("unit_type <", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeLessThanOrEqualTo(String value) {
            addCriterion("unit_type <=", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeLike(String value) {
            addCriterion("unit_type like", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotLike(String value) {
            addCriterion("unit_type not like", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeIn(List<String> values) {
            addCriterion("unit_type in", values, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotIn(List<String> values) {
            addCriterion("unit_type not in", values, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeBetween(String value1, String value2) {
            addCriterion("unit_type between", value1, value2, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotBetween(String value1, String value2) {
            addCriterion("unit_type not between", value1, value2, "unitType");
            return (Criteria) this;
        }

        public Criteria andIncomeIsNull() {
            addCriterion("income is null");
            return (Criteria) this;
        }

        public Criteria andIncomeIsNotNull() {
            addCriterion("income is not null");
            return (Criteria) this;
        }

        public Criteria andIncomeEqualTo(String value) {
            addCriterion("income =", value, "income");
            return (Criteria) this;
        }

        public Criteria andIncomeNotEqualTo(String value) {
            addCriterion("income <>", value, "income");
            return (Criteria) this;
        }

        public Criteria andIncomeGreaterThan(String value) {
            addCriterion("income >", value, "income");
            return (Criteria) this;
        }

        public Criteria andIncomeGreaterThanOrEqualTo(String value) {
            addCriterion("income >=", value, "income");
            return (Criteria) this;
        }

        public Criteria andIncomeLessThan(String value) {
            addCriterion("income <", value, "income");
            return (Criteria) this;
        }

        public Criteria andIncomeLessThanOrEqualTo(String value) {
            addCriterion("income <=", value, "income");
            return (Criteria) this;
        }

        public Criteria andIncomeLike(String value) {
            addCriterion("income like", value, "income");
            return (Criteria) this;
        }

        public Criteria andIncomeNotLike(String value) {
            addCriterion("income not like", value, "income");
            return (Criteria) this;
        }

        public Criteria andIncomeIn(List<String> values) {
            addCriterion("income in", values, "income");
            return (Criteria) this;
        }

        public Criteria andIncomeNotIn(List<String> values) {
            addCriterion("income not in", values, "income");
            return (Criteria) this;
        }

        public Criteria andIncomeBetween(String value1, String value2) {
            addCriterion("income between", value1, value2, "income");
            return (Criteria) this;
        }

        public Criteria andIncomeNotBetween(String value1, String value2) {
            addCriterion("income not between", value1, value2, "income");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedIsNull() {
            addCriterion("income_speed is null");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedIsNotNull() {
            addCriterion("income_speed is not null");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedEqualTo(String value) {
            addCriterion("income_speed =", value, "incomeSpeed");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedNotEqualTo(String value) {
            addCriterion("income_speed <>", value, "incomeSpeed");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedGreaterThan(String value) {
            addCriterion("income_speed >", value, "incomeSpeed");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedGreaterThanOrEqualTo(String value) {
            addCriterion("income_speed >=", value, "incomeSpeed");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedLessThan(String value) {
            addCriterion("income_speed <", value, "incomeSpeed");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedLessThanOrEqualTo(String value) {
            addCriterion("income_speed <=", value, "incomeSpeed");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedLike(String value) {
            addCriterion("income_speed like", value, "incomeSpeed");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedNotLike(String value) {
            addCriterion("income_speed not like", value, "incomeSpeed");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedIn(List<String> values) {
            addCriterion("income_speed in", values, "incomeSpeed");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedNotIn(List<String> values) {
            addCriterion("income_speed not in", values, "incomeSpeed");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedBetween(String value1, String value2) {
            addCriterion("income_speed between", value1, value2, "incomeSpeed");
            return (Criteria) this;
        }

        public Criteria andIncomeSpeedNotBetween(String value1, String value2) {
            addCriterion("income_speed not between", value1, value2, "incomeSpeed");
            return (Criteria) this;
        }

        public Criteria andProfitIsNull() {
            addCriterion("profit is null");
            return (Criteria) this;
        }

        public Criteria andProfitIsNotNull() {
            addCriterion("profit is not null");
            return (Criteria) this;
        }

        public Criteria andProfitEqualTo(String value) {
            addCriterion("profit =", value, "profit");
            return (Criteria) this;
        }

        public Criteria andProfitNotEqualTo(String value) {
            addCriterion("profit <>", value, "profit");
            return (Criteria) this;
        }

        public Criteria andProfitGreaterThan(String value) {
            addCriterion("profit >", value, "profit");
            return (Criteria) this;
        }

        public Criteria andProfitGreaterThanOrEqualTo(String value) {
            addCriterion("profit >=", value, "profit");
            return (Criteria) this;
        }

        public Criteria andProfitLessThan(String value) {
            addCriterion("profit <", value, "profit");
            return (Criteria) this;
        }

        public Criteria andProfitLessThanOrEqualTo(String value) {
            addCriterion("profit <=", value, "profit");
            return (Criteria) this;
        }

        public Criteria andProfitLike(String value) {
            addCriterion("profit like", value, "profit");
            return (Criteria) this;
        }

        public Criteria andProfitNotLike(String value) {
            addCriterion("profit not like", value, "profit");
            return (Criteria) this;
        }

        public Criteria andProfitIn(List<String> values) {
            addCriterion("profit in", values, "profit");
            return (Criteria) this;
        }

        public Criteria andProfitNotIn(List<String> values) {
            addCriterion("profit not in", values, "profit");
            return (Criteria) this;
        }

        public Criteria andProfitBetween(String value1, String value2) {
            addCriterion("profit between", value1, value2, "profit");
            return (Criteria) this;
        }

        public Criteria andProfitNotBetween(String value1, String value2) {
            addCriterion("profit not between", value1, value2, "profit");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedIsNull() {
            addCriterion("profit_speed is null");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedIsNotNull() {
            addCriterion("profit_speed is not null");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedEqualTo(String value) {
            addCriterion("profit_speed =", value, "profitSpeed");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedNotEqualTo(String value) {
            addCriterion("profit_speed <>", value, "profitSpeed");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedGreaterThan(String value) {
            addCriterion("profit_speed >", value, "profitSpeed");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedGreaterThanOrEqualTo(String value) {
            addCriterion("profit_speed >=", value, "profitSpeed");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedLessThan(String value) {
            addCriterion("profit_speed <", value, "profitSpeed");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedLessThanOrEqualTo(String value) {
            addCriterion("profit_speed <=", value, "profitSpeed");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedLike(String value) {
            addCriterion("profit_speed like", value, "profitSpeed");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedNotLike(String value) {
            addCriterion("profit_speed not like", value, "profitSpeed");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedIn(List<String> values) {
            addCriterion("profit_speed in", values, "profitSpeed");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedNotIn(List<String> values) {
            addCriterion("profit_speed not in", values, "profitSpeed");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedBetween(String value1, String value2) {
            addCriterion("profit_speed between", value1, value2, "profitSpeed");
            return (Criteria) this;
        }

        public Criteria andProfitSpeedNotBetween(String value1, String value2) {
            addCriterion("profit_speed not between", value1, value2, "profitSpeed");
            return (Criteria) this;
        }

        public Criteria andAverageNumberIsNull() {
            addCriterion("average_number is null");
            return (Criteria) this;
        }

        public Criteria andAverageNumberIsNotNull() {
            addCriterion("average_number is not null");
            return (Criteria) this;
        }

        public Criteria andAverageNumberEqualTo(String value) {
            addCriterion("average_number =", value, "averageNumber");
            return (Criteria) this;
        }

        public Criteria andAverageNumberNotEqualTo(String value) {
            addCriterion("average_number <>", value, "averageNumber");
            return (Criteria) this;
        }

        public Criteria andAverageNumberGreaterThan(String value) {
            addCriterion("average_number >", value, "averageNumber");
            return (Criteria) this;
        }

        public Criteria andAverageNumberGreaterThanOrEqualTo(String value) {
            addCriterion("average_number >=", value, "averageNumber");
            return (Criteria) this;
        }

        public Criteria andAverageNumberLessThan(String value) {
            addCriterion("average_number <", value, "averageNumber");
            return (Criteria) this;
        }

        public Criteria andAverageNumberLessThanOrEqualTo(String value) {
            addCriterion("average_number <=", value, "averageNumber");
            return (Criteria) this;
        }

        public Criteria andAverageNumberLike(String value) {
            addCriterion("average_number like", value, "averageNumber");
            return (Criteria) this;
        }

        public Criteria andAverageNumberNotLike(String value) {
            addCriterion("average_number not like", value, "averageNumber");
            return (Criteria) this;
        }

        public Criteria andAverageNumberIn(List<String> values) {
            addCriterion("average_number in", values, "averageNumber");
            return (Criteria) this;
        }

        public Criteria andAverageNumberNotIn(List<String> values) {
            addCriterion("average_number not in", values, "averageNumber");
            return (Criteria) this;
        }

        public Criteria andAverageNumberBetween(String value1, String value2) {
            addCriterion("average_number between", value1, value2, "averageNumber");
            return (Criteria) this;
        }

        public Criteria andAverageNumberNotBetween(String value1, String value2) {
            addCriterion("average_number not between", value1, value2, "averageNumber");
            return (Criteria) this;
        }

        public Criteria andPeriodIsNull() {
            addCriterion("period is null");
            return (Criteria) this;
        }

        public Criteria andPeriodIsNotNull() {
            addCriterion("period is not null");
            return (Criteria) this;
        }

        public Criteria andPeriodEqualTo(String value) {
            addCriterion("period =", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotEqualTo(String value) {
            addCriterion("period <>", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodGreaterThan(String value) {
            addCriterion("period >", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodGreaterThanOrEqualTo(String value) {
            addCriterion("period >=", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLessThan(String value) {
            addCriterion("period <", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLessThanOrEqualTo(String value) {
            addCriterion("period <=", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLike(String value) {
            addCriterion("period like", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotLike(String value) {
            addCriterion("period not like", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodIn(List<String> values) {
            addCriterion("period in", values, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotIn(List<String> values) {
            addCriterion("period not in", values, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodBetween(String value1, String value2) {
            addCriterion("period between", value1, value2, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotBetween(String value1, String value2) {
            addCriterion("period not between", value1, value2, "period");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedIsNull() {
            addCriterion("number_speed is null");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedIsNotNull() {
            addCriterion("number_speed is not null");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedEqualTo(String value) {
            addCriterion("number_speed =", value, "numberSpeed");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedNotEqualTo(String value) {
            addCriterion("number_speed <>", value, "numberSpeed");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedGreaterThan(String value) {
            addCriterion("number_speed >", value, "numberSpeed");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedGreaterThanOrEqualTo(String value) {
            addCriterion("number_speed >=", value, "numberSpeed");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedLessThan(String value) {
            addCriterion("number_speed <", value, "numberSpeed");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedLessThanOrEqualTo(String value) {
            addCriterion("number_speed <=", value, "numberSpeed");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedLike(String value) {
            addCriterion("number_speed like", value, "numberSpeed");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedNotLike(String value) {
            addCriterion("number_speed not like", value, "numberSpeed");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedIn(List<String> values) {
            addCriterion("number_speed in", values, "numberSpeed");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedNotIn(List<String> values) {
            addCriterion("number_speed not in", values, "numberSpeed");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedBetween(String value1, String value2) {
            addCriterion("number_speed between", value1, value2, "numberSpeed");
            return (Criteria) this;
        }

        public Criteria andNumberSpeedNotBetween(String value1, String value2) {
            addCriterion("number_speed not between", value1, value2, "numberSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueIsNull() {
            addCriterion("increase_value is null");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueIsNotNull() {
            addCriterion("increase_value is not null");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueEqualTo(String value) {
            addCriterion("increase_value =", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueNotEqualTo(String value) {
            addCriterion("increase_value <>", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueGreaterThan(String value) {
            addCriterion("increase_value >", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueGreaterThanOrEqualTo(String value) {
            addCriterion("increase_value >=", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueLessThan(String value) {
            addCriterion("increase_value <", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueLessThanOrEqualTo(String value) {
            addCriterion("increase_value <=", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueLike(String value) {
            addCriterion("increase_value like", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueNotLike(String value) {
            addCriterion("increase_value not like", value, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueIn(List<String> values) {
            addCriterion("increase_value in", values, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueNotIn(List<String> values) {
            addCriterion("increase_value not in", values, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueBetween(String value1, String value2) {
            addCriterion("increase_value between", value1, value2, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseValueNotBetween(String value1, String value2) {
            addCriterion("increase_value not between", value1, value2, "increaseValue");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedIsNull() {
            addCriterion("increase_speed is null");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedIsNotNull() {
            addCriterion("increase_speed is not null");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedEqualTo(String value) {
            addCriterion("increase_speed =", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedNotEqualTo(String value) {
            addCriterion("increase_speed <>", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedGreaterThan(String value) {
            addCriterion("increase_speed >", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedGreaterThanOrEqualTo(String value) {
            addCriterion("increase_speed >=", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedLessThan(String value) {
            addCriterion("increase_speed <", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedLessThanOrEqualTo(String value) {
            addCriterion("increase_speed <=", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedLike(String value) {
            addCriterion("increase_speed like", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedNotLike(String value) {
            addCriterion("increase_speed not like", value, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedIn(List<String> values) {
            addCriterion("increase_speed in", values, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedNotIn(List<String> values) {
            addCriterion("increase_speed not in", values, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedBetween(String value1, String value2) {
            addCriterion("increase_speed between", value1, value2, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andIncreaseSpeedNotBetween(String value1, String value2) {
            addCriterion("increase_speed not between", value1, value2, "increaseSpeed");
            return (Criteria) this;
        }

        public Criteria andFileIsNull() {
            addCriterion("file is null");
            return (Criteria) this;
        }

        public Criteria andFileIsNotNull() {
            addCriterion("file is not null");
            return (Criteria) this;
        }

        public Criteria andFileEqualTo(String value) {
            addCriterion("file =", value, "file");
            return (Criteria) this;
        }

        public Criteria andFileNotEqualTo(String value) {
            addCriterion("file <>", value, "file");
            return (Criteria) this;
        }

        public Criteria andFileGreaterThan(String value) {
            addCriterion("file >", value, "file");
            return (Criteria) this;
        }

        public Criteria andFileGreaterThanOrEqualTo(String value) {
            addCriterion("file >=", value, "file");
            return (Criteria) this;
        }

        public Criteria andFileLessThan(String value) {
            addCriterion("file <", value, "file");
            return (Criteria) this;
        }

        public Criteria andFileLessThanOrEqualTo(String value) {
            addCriterion("file <=", value, "file");
            return (Criteria) this;
        }

        public Criteria andFileLike(String value) {
            addCriterion("file like", value, "file");
            return (Criteria) this;
        }

        public Criteria andFileNotLike(String value) {
            addCriterion("file not like", value, "file");
            return (Criteria) this;
        }

        public Criteria andFileIn(List<String> values) {
            addCriterion("file in", values, "file");
            return (Criteria) this;
        }

        public Criteria andFileNotIn(List<String> values) {
            addCriterion("file not in", values, "file");
            return (Criteria) this;
        }

        public Criteria andFileBetween(String value1, String value2) {
            addCriterion("file between", value1, value2, "file");
            return (Criteria) this;
        }

        public Criteria andFileNotBetween(String value1, String value2) {
            addCriterion("file not between", value1, value2, "file");
            return (Criteria) this;
        }

        public Criteria andFileidIsNull() {
            addCriterion("fileid is null");
            return (Criteria) this;
        }

        public Criteria andFileidIsNotNull() {
            addCriterion("fileid is not null");
            return (Criteria) this;
        }

        public Criteria andFileidEqualTo(String value) {
            addCriterion("fileid =", value, "fileid");
            return (Criteria) this;
        }

        public Criteria andFileidNotEqualTo(String value) {
            addCriterion("fileid <>", value, "fileid");
            return (Criteria) this;
        }

        public Criteria andFileidGreaterThan(String value) {
            addCriterion("fileid >", value, "fileid");
            return (Criteria) this;
        }

        public Criteria andFileidGreaterThanOrEqualTo(String value) {
            addCriterion("fileid >=", value, "fileid");
            return (Criteria) this;
        }

        public Criteria andFileidLessThan(String value) {
            addCriterion("fileid <", value, "fileid");
            return (Criteria) this;
        }

        public Criteria andFileidLessThanOrEqualTo(String value) {
            addCriterion("fileid <=", value, "fileid");
            return (Criteria) this;
        }

        public Criteria andFileidLike(String value) {
            addCriterion("fileid like", value, "fileid");
            return (Criteria) this;
        }

        public Criteria andFileidNotLike(String value) {
            addCriterion("fileid not like", value, "fileid");
            return (Criteria) this;
        }

        public Criteria andFileidIn(List<String> values) {
            addCriterion("fileid in", values, "fileid");
            return (Criteria) this;
        }

        public Criteria andFileidNotIn(List<String> values) {
            addCriterion("fileid not in", values, "fileid");
            return (Criteria) this;
        }

        public Criteria andFileidBetween(String value1, String value2) {
            addCriterion("fileid between", value1, value2, "fileid");
            return (Criteria) this;
        }

        public Criteria andFileidNotBetween(String value1, String value2) {
            addCriterion("fileid not between", value1, value2, "fileid");
            return (Criteria) this;
        }

        public Criteria andFilenameIsNull() {
            addCriterion("filename is null");
            return (Criteria) this;
        }

        public Criteria andFilenameIsNotNull() {
            addCriterion("filename is not null");
            return (Criteria) this;
        }

        public Criteria andFilenameEqualTo(String value) {
            addCriterion("filename =", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameNotEqualTo(String value) {
            addCriterion("filename <>", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameGreaterThan(String value) {
            addCriterion("filename >", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameGreaterThanOrEqualTo(String value) {
            addCriterion("filename >=", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameLessThan(String value) {
            addCriterion("filename <", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameLessThanOrEqualTo(String value) {
            addCriterion("filename <=", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameLike(String value) {
            addCriterion("filename like", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameNotLike(String value) {
            addCriterion("filename not like", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameIn(List<String> values) {
            addCriterion("filename in", values, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameNotIn(List<String> values) {
            addCriterion("filename not in", values, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameBetween(String value1, String value2) {
            addCriterion("filename between", value1, value2, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameNotBetween(String value1, String value2) {
            addCriterion("filename not between", value1, value2, "filename");
            return (Criteria) this;
        }

        public Criteria andImportTimeIsNull() {
            addCriterion("import_time is null");
            return (Criteria) this;
        }

        public Criteria andImportTimeIsNotNull() {
            addCriterion("import_time is not null");
            return (Criteria) this;
        }

        public Criteria andImportTimeEqualTo(Date value) {
            addCriterion("import_time =", value, "importTime");
            return (Criteria) this;
        }

        public Criteria andImportTimeNotEqualTo(Date value) {
            addCriterion("import_time <>", value, "importTime");
            return (Criteria) this;
        }

        public Criteria andImportTimeGreaterThan(Date value) {
            addCriterion("import_time >", value, "importTime");
            return (Criteria) this;
        }

        public Criteria andImportTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("import_time >=", value, "importTime");
            return (Criteria) this;
        }

        public Criteria andImportTimeLessThan(Date value) {
            addCriterion("import_time <", value, "importTime");
            return (Criteria) this;
        }

        public Criteria andImportTimeLessThanOrEqualTo(Date value) {
            addCriterion("import_time <=", value, "importTime");
            return (Criteria) this;
        }

        public Criteria andImportTimeIn(List<Date> values) {
            addCriterion("import_time in", values, "importTime");
            return (Criteria) this;
        }

        public Criteria andImportTimeNotIn(List<Date> values) {
            addCriterion("import_time not in", values, "importTime");
            return (Criteria) this;
        }

        public Criteria andImportTimeBetween(Date value1, Date value2) {
            addCriterion("import_time between", value1, value2, "importTime");
            return (Criteria) this;
        }

        public Criteria andImportTimeNotBetween(Date value1, Date value2) {
            addCriterion("import_time not between", value1, value2, "importTime");
            return (Criteria) this;
        }

        public Criteria andImporterIsNull() {
            addCriterion("importer is null");
            return (Criteria) this;
        }

        public Criteria andImporterIsNotNull() {
            addCriterion("importer is not null");
            return (Criteria) this;
        }

        public Criteria andImporterEqualTo(String value) {
            addCriterion("importer =", value, "importer");
            return (Criteria) this;
        }

        public Criteria andImporterNotEqualTo(String value) {
            addCriterion("importer <>", value, "importer");
            return (Criteria) this;
        }

        public Criteria andImporterGreaterThan(String value) {
            addCriterion("importer >", value, "importer");
            return (Criteria) this;
        }

        public Criteria andImporterGreaterThanOrEqualTo(String value) {
            addCriterion("importer >=", value, "importer");
            return (Criteria) this;
        }

        public Criteria andImporterLessThan(String value) {
            addCriterion("importer <", value, "importer");
            return (Criteria) this;
        }

        public Criteria andImporterLessThanOrEqualTo(String value) {
            addCriterion("importer <=", value, "importer");
            return (Criteria) this;
        }

        public Criteria andImporterLike(String value) {
            addCriterion("importer like", value, "importer");
            return (Criteria) this;
        }

        public Criteria andImporterNotLike(String value) {
            addCriterion("importer not like", value, "importer");
            return (Criteria) this;
        }

        public Criteria andImporterIn(List<String> values) {
            addCriterion("importer in", values, "importer");
            return (Criteria) this;
        }

        public Criteria andImporterNotIn(List<String> values) {
            addCriterion("importer not in", values, "importer");
            return (Criteria) this;
        }

        public Criteria andImporterBetween(String value1, String value2) {
            addCriterion("importer between", value1, value2, "importer");
            return (Criteria) this;
        }

        public Criteria andImporterNotBetween(String value1, String value2) {
            addCriterion("importer not between", value1, value2, "importer");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}