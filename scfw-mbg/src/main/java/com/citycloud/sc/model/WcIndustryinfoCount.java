package com.citycloud.sc.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;

public class WcIndustryinfoCount implements Serializable {
    @ApiModelProperty(value = "主键ID")
    private Integer id;

    @ApiModelProperty(value = "年度")
    private String year;

    @ApiModelProperty(value = "行业代码")
    private String industryCode;

    @ApiModelProperty(value = "行业名称")
    private String industryName;

    @ApiModelProperty(value = "年度增加值")
    private BigDecimal increaseValue;

    @ApiModelProperty(value = "去年同期")
    private Integer period;

    @ApiModelProperty(value = "增速(%)")
    private BigDecimal increaseSpeed;

    @ApiModelProperty(value = "企业数量")
    private Integer companyNumber;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }

    public BigDecimal getIncreaseValue() {
        return increaseValue;
    }

    public void setIncreaseValue(BigDecimal increaseValue) {
        this.increaseValue = increaseValue;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public BigDecimal getIncreaseSpeed() {
        return increaseSpeed;
    }

    public void setIncreaseSpeed(BigDecimal increaseSpeed) {
        this.increaseSpeed = increaseSpeed;
    }

    public Integer getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyNumber(Integer companyNumber) {
        this.companyNumber = companyNumber;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", year=").append(year);
        sb.append(", industryCode=").append(industryCode);
        sb.append(", industryName=").append(industryName);
        sb.append(", increaseValue=").append(increaseValue);
        sb.append(", period=").append(period);
        sb.append(", increaseSpeed=").append(increaseSpeed);
        sb.append(", companyNumber=").append(companyNumber);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}