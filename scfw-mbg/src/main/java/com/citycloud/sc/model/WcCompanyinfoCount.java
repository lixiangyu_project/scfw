package com.citycloud.sc.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

public class WcCompanyinfoCount implements Serializable {
    @ApiModelProperty(value = "主键ID")
    private Integer id;

    @ApiModelProperty(value = "年度")
    private String year;

    @ApiModelProperty(value = "属地街道")
    private String street;

    @ApiModelProperty(value = "企业名称")
    private String name;

    @ApiModelProperty(value = "统一信用代码")
    private String creditCode;

    @ApiModelProperty(value = "年度增加值")
    private String increaseValue;

    @ApiModelProperty(value = "去年同期")
    private String period;

    @ApiModelProperty(value = "增速")
    private String increaseSpeed;

    @ApiModelProperty(value = "扶持项目数量")
    private Integer supportProjectnum;

    @ApiModelProperty(value = "扶持金额（万元）")
    private String supportMonenynum;

    @ApiModelProperty(value = "税收收入本年（万元）")
    private String taxIncome;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreditCode() {
        return creditCode;
    }

    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode;
    }

    public String getIncreaseValue() {
        return increaseValue;
    }

    public void setIncreaseValue(String increaseValue) {
        this.increaseValue = increaseValue;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getIncreaseSpeed() {
        return increaseSpeed;
    }

    public void setIncreaseSpeed(String increaseSpeed) {
        this.increaseSpeed = increaseSpeed;
    }

    public Integer getSupportProjectnum() {
        return supportProjectnum;
    }

    public void setSupportProjectnum(Integer supportProjectnum) {
        this.supportProjectnum = supportProjectnum;
    }

    public String getSupportMonenynum() {
        return supportMonenynum;
    }

    public void setSupportMonenynum(String supportMonenynum) {
        this.supportMonenynum = supportMonenynum;
    }

    public String getTaxIncome() {
        return taxIncome;
    }

    public void setTaxIncome(String taxIncome) {
        this.taxIncome = taxIncome;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", year=").append(year);
        sb.append(", street=").append(street);
        sb.append(", name=").append(name);
        sb.append(", creditCode=").append(creditCode);
        sb.append(", increaseValue=").append(increaseValue);
        sb.append(", period=").append(period);
        sb.append(", increaseSpeed=").append(increaseSpeed);
        sb.append(", supportProjectnum=").append(supportProjectnum);
        sb.append(", supportMonenynum=").append(supportMonenynum);
        sb.append(", taxIncome=").append(taxIncome);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}