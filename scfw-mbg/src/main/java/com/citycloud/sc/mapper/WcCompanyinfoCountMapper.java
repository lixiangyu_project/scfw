package com.citycloud.sc.mapper;

import com.citycloud.sc.model.WcCompanyinfoCount;
import com.citycloud.sc.model.WcCompanyinfoCountExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WcCompanyinfoCountMapper {
    long countByExample(WcCompanyinfoCountExample example);

    int deleteByExample(WcCompanyinfoCountExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WcCompanyinfoCount record);

    int insertSelective(WcCompanyinfoCount record);

    List<WcCompanyinfoCount> selectByExample(WcCompanyinfoCountExample example);

    WcCompanyinfoCount selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WcCompanyinfoCount record, @Param("example") WcCompanyinfoCountExample example);

    int updateByExample(@Param("record") WcCompanyinfoCount record, @Param("example") WcCompanyinfoCountExample example);

    int updateByPrimaryKeySelective(WcCompanyinfoCount record);

    int updateByPrimaryKey(WcCompanyinfoCount record);
}