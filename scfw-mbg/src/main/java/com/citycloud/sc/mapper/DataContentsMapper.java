package com.citycloud.sc.mapper;

import com.citycloud.sc.model.DataContents;
import com.citycloud.sc.model.DataContentsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DataContentsMapper {
    long countByExample(DataContentsExample example);

    int deleteByExample(DataContentsExample example);

    int deleteByPrimaryKey(String id);

    int insert(DataContents record);

    int insertSelective(DataContents record);

    List<DataContents> selectByExample(DataContentsExample example);

    DataContents selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") DataContents record, @Param("example") DataContentsExample example);

    int updateByExample(@Param("record") DataContents record, @Param("example") DataContentsExample example);

    int updateByPrimaryKeySelective(DataContents record);

    int updateByPrimaryKey(DataContents record);
}