package com.citycloud.sc.mapper;

import com.citycloud.sc.model.WcStreetinfoCount;
import com.citycloud.sc.model.WcStreetinfoCountExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WcStreetinfoCountMapper {
    long countByExample(WcStreetinfoCountExample example);

    int deleteByExample(WcStreetinfoCountExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WcStreetinfoCount record);

    int insertSelective(WcStreetinfoCount record);

    List<WcStreetinfoCount> selectByExample(WcStreetinfoCountExample example);

    WcStreetinfoCount selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WcStreetinfoCount record, @Param("example") WcStreetinfoCountExample example);

    int updateByExample(@Param("record") WcStreetinfoCount record, @Param("example") WcStreetinfoCountExample example);

    int updateByPrimaryKeySelective(WcStreetinfoCount record);

    int updateByPrimaryKey(WcStreetinfoCount record);
}