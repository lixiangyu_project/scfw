package com.citycloud.sc.mapper;

import com.citycloud.sc.model.WcProjectInfo;
import com.citycloud.sc.model.WcProjectInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WcProjectInfoMapper {
    long countByExample(WcProjectInfoExample example);

    int deleteByExample(WcProjectInfoExample example);

    int deleteByPrimaryKey(String id);

    int insert(WcProjectInfo record);

    int insertSelective(WcProjectInfo record);

    List<WcProjectInfo> selectByExample(WcProjectInfoExample example);

    WcProjectInfo selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") WcProjectInfo record, @Param("example") WcProjectInfoExample example);

    int updateByExample(@Param("record") WcProjectInfo record, @Param("example") WcProjectInfoExample example);

    int updateByPrimaryKeySelective(WcProjectInfo record);

    int updateByPrimaryKey(WcProjectInfo record);
}