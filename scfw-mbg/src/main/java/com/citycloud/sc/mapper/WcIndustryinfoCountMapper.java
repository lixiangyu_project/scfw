package com.citycloud.sc.mapper;

import com.citycloud.sc.model.WcIndustryinfoCount;
import com.citycloud.sc.model.WcIndustryinfoCountExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WcIndustryinfoCountMapper {
    long countByExample(WcIndustryinfoCountExample example);

    int deleteByExample(WcIndustryinfoCountExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WcIndustryinfoCount record);

    int insertSelective(WcIndustryinfoCount record);

    List<WcIndustryinfoCount> selectByExample(WcIndustryinfoCountExample example);

    WcIndustryinfoCount selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WcIndustryinfoCount record, @Param("example") WcIndustryinfoCountExample example);

    int updateByExample(@Param("record") WcIndustryinfoCount record, @Param("example") WcIndustryinfoCountExample example);

    int updateByPrimaryKeySelective(WcIndustryinfoCount record);

    int updateByPrimaryKey(WcIndustryinfoCount record);
}