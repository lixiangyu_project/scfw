package com.citycloud.sc.mapper;

import com.citycloud.sc.model.WcCompanyTaxation;
import com.citycloud.sc.model.WcCompanyTaxationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WcCompanyTaxationMapper {
    long countByExample(WcCompanyTaxationExample example);

    int deleteByExample(WcCompanyTaxationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WcCompanyTaxation record);

    int insertSelective(WcCompanyTaxation record);

    List<WcCompanyTaxation> selectByExample(WcCompanyTaxationExample example);

    WcCompanyTaxation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WcCompanyTaxation record, @Param("example") WcCompanyTaxationExample example);

    int updateByExample(@Param("record") WcCompanyTaxation record, @Param("example") WcCompanyTaxationExample example);

    int updateByPrimaryKeySelective(WcCompanyTaxation record);

    int updateByPrimaryKey(WcCompanyTaxation record);
}