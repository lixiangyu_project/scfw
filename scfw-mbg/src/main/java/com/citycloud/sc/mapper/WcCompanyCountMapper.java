package com.citycloud.sc.mapper;

import com.citycloud.sc.model.WcCompanyCount;
import com.citycloud.sc.model.WcCompanyCountExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WcCompanyCountMapper {
    long countByExample(WcCompanyCountExample example);

    int deleteByExample(WcCompanyCountExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WcCompanyCount record);

    int insertSelective(WcCompanyCount record);

    List<WcCompanyCount> selectByExample(WcCompanyCountExample example);

    WcCompanyCount selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WcCompanyCount record, @Param("example") WcCompanyCountExample example);

    int updateByExample(@Param("record") WcCompanyCount record, @Param("example") WcCompanyCountExample example);

    int updateByPrimaryKeySelective(WcCompanyCount record);

    int updateByPrimaryKey(WcCompanyCount record);
}