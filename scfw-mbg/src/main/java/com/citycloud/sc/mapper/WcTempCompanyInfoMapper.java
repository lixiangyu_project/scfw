package com.citycloud.sc.mapper;

import com.citycloud.sc.model.WcTempCompanyInfo;
import com.citycloud.sc.model.WcTempCompanyInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WcTempCompanyInfoMapper {
    long countByExample(WcTempCompanyInfoExample example);

    int deleteByExample(WcTempCompanyInfoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WcTempCompanyInfo record);

    int insertSelective(WcTempCompanyInfo record);

    List<WcTempCompanyInfo> selectByExample(WcTempCompanyInfoExample example);

    WcTempCompanyInfo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WcTempCompanyInfo record, @Param("example") WcTempCompanyInfoExample example);

    int updateByExample(@Param("record") WcTempCompanyInfo record, @Param("example") WcTempCompanyInfoExample example);

    int updateByPrimaryKeySelective(WcTempCompanyInfo record);

    int updateByPrimaryKey(WcTempCompanyInfo record);
}