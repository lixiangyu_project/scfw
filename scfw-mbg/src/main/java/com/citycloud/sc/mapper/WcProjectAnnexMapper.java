package com.citycloud.sc.mapper;

import com.citycloud.sc.model.WcProjectAnnex;
import com.citycloud.sc.model.WcProjectAnnexExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WcProjectAnnexMapper {
    long countByExample(WcProjectAnnexExample example);

    int deleteByExample(WcProjectAnnexExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WcProjectAnnex record);

    int insertSelective(WcProjectAnnex record);

    List<WcProjectAnnex> selectByExample(WcProjectAnnexExample example);

    WcProjectAnnex selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WcProjectAnnex record, @Param("example") WcProjectAnnexExample example);

    int updateByExample(@Param("record") WcProjectAnnex record, @Param("example") WcProjectAnnexExample example);

    int updateByPrimaryKeySelective(WcProjectAnnex record);

    int updateByPrimaryKey(WcProjectAnnex record);
}