package com.citycloud.sc.mapper;

import com.citycloud.sc.model.WcCompanyInfo;
import com.citycloud.sc.model.WcCompanyInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WcCompanyInfoMapper {
    long countByExample(WcCompanyInfoExample example);

    int deleteByExample(WcCompanyInfoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WcCompanyInfo record);

    int insertSelective(WcCompanyInfo record);

    List<WcCompanyInfo> selectByExample(WcCompanyInfoExample example);

    WcCompanyInfo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WcCompanyInfo record, @Param("example") WcCompanyInfoExample example);

    int updateByExample(@Param("record") WcCompanyInfo record, @Param("example") WcCompanyInfoExample example);

    int updateByPrimaryKeySelective(WcCompanyInfo record);

    int updateByPrimaryKey(WcCompanyInfo record);
}