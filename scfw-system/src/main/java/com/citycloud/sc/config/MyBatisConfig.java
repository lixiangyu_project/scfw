package com.citycloud.sc.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * MyBatis配置类
 */
@Configuration
@EnableTransactionManagement
@MapperScan({"com.citycloud.sc.mapper","com.citycloud.sc.dao"})
public class MyBatisConfig {
}
