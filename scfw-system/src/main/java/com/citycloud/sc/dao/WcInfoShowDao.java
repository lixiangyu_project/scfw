package com.citycloud.sc.dao;

import com.citycloud.sc.dto.WcStreetInfoParam;
import com.citycloud.sc.dto.WcStreetInfoResult;
import com.citycloud.sc.model.WcCompanyinfoCount;
import com.citycloud.sc.model.WcIndustryinfoCount;
import com.citycloud.sc.model.WcStreetinfoCount;

import java.util.List;

/**
 * create by: lixy
 * description: 自定义dao层
 * create time: 2019/10/14 17:19
 * @params  * @Param: null
 * @return
 */
public interface WcInfoShowDao {

    /**
     * 根据行业统计企业信息
     */
     List<WcIndustryinfoCount> getInfoByIndustry();

    /**
     * 获得街道统计和税收数据
     */
    List<WcStreetinfoCount> getCountAndTaxByStreet();

    /**
     * 按年度统计街道信息
     */
    List<WcStreetInfoResult> countStreetInfo(WcStreetInfoParam wcStreetInfoParam);
}
