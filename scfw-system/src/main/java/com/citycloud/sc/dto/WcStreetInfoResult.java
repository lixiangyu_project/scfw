package com.citycloud.sc.dto;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 * create by: lixy
 * description: 文创街道信息返回结果
 * create time: 2019/10/17 14:04
 * @return
 */
public class WcStreetInfoResult {

    @ApiModelProperty(value = "年度")
    private String year;

    @ApiModelProperty(value = "属地街道")
    private String street;

    @ApiModelProperty(value = "增加值(万元)")
    private BigDecimal increaseValue;

    @ApiModelProperty(value = "年度税收收入（万元）")
    private BigDecimal taxIncome;

    @ApiModelProperty(value = "扶持企业数量")
    private Integer supportCompanynum;

    @ApiModelProperty(value = "扶持项目数量")
    private Integer supportProjectnum;

    @ApiModelProperty(value = "扶持金额（万元）")
    private BigDecimal supportMonenynum;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public BigDecimal getIncreaseValue() {
        return increaseValue;
    }

    public void setIncreaseValue(BigDecimal increaseValue) {
        this.increaseValue = increaseValue;
    }

    public BigDecimal getTaxIncome() {
        return taxIncome;
    }

    public void setTaxIncome(BigDecimal taxIncome) {
        this.taxIncome = taxIncome;
    }

    public Integer getSupportCompanynum() {
        return supportCompanynum;
    }

    public void setSupportCompanynum(Integer supportCompanynum) {
        this.supportCompanynum = supportCompanynum;
    }

    public Integer getSupportProjectnum() {
        return supportProjectnum;
    }

    public void setSupportProjectnum(Integer supportProjectnum) {
        this.supportProjectnum = supportProjectnum;
    }

    public BigDecimal getSupportMonenynum() {
        return supportMonenynum;
    }

    public void setSupportMonenynum(BigDecimal supportMonenynum) {
        this.supportMonenynum = supportMonenynum;
    }
}
