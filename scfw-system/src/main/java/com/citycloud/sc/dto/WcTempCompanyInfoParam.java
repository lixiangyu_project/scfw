package com.citycloud.sc.dto;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @ClassName WcTempCompanyInfoParam
 * @Description 临时企业传递参数
 * @Author 相信飞鱼
 * @Date 2019/10/11 11:10
 * @Version 1.0
 **/
public class WcTempCompanyInfoParam {
    @ApiModelProperty(value = "企业名称",required = true)
    @NotEmpty(message = "名称不能为空")
    private String name;
    @ApiModelProperty(value = "统一信用代码")
    private String credit_code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCredit_code() {
        return credit_code;
    }

    public void setCredit_code(String credit_code) {
        this.credit_code = credit_code;
    }
}
