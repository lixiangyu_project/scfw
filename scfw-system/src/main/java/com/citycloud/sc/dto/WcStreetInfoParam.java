package com.citycloud.sc.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 文创街道入参
 */
@Getter
@Setter
public class WcStreetInfoParam {
    @ApiModelProperty(value = "年度")
    private String year;
    @ApiModelProperty(value = "属地街道")
    private String street;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
