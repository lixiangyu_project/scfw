package com.citycloud.sc.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

/**
 * @ClassName UploadController
 * @Description TODO
 * @Author 相信飞鱼
 * @Date 2019/10/11 16:25
 * @Version 1.0
 **/
@Controller
@Api(tags = "UploadController",description = "文件上传")
@RequestMapping("/upload")
public class UploadController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UploadController.class);

    @ResponseBody
    @RequestMapping(value = "/wcProject",method = RequestMethod.POST)
    @ApiOperation("上传文创项目文件")
    public String wcProject(@RequestParam("file") MultipartFile file) {

        if (file.isEmpty()) {
            return "上传失败，请选择文件";
        }
        String fileName = file.getOriginalFilename();
        String filePath = "D:/usr/local/tomcat7/webapps/snow6/file/";
        File dest = new File(filePath + fileName);
        System.out.println("filename:"+fileName);
        System.out.println("name:"+file.getName());
        System.out.println("type:"+file.getContentType());
        try {
            file.transferTo(dest);
            LOGGER.info("上传成功");
            return "上传成功";
        } catch (IOException e) {
            LOGGER.error(e.toString(), e);
        }
        return "上传失败！";
    }

    @GetMapping("/page")
    public String upload() {
        return "uploadPage";
    }

    @GetMapping("/download")
    public String download() {
        return "downloadFile";
    }
}
