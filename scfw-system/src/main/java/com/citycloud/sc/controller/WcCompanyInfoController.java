package com.citycloud.sc.controller;

import com.citycloud.sc.common.api.CommonResult;
import com.citycloud.sc.model.WcCompanyInfo;
import com.citycloud.sc.service.WcCompanyInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @ClassName WcCompanyInfoController
 * @Description TODO
 * @Author lixy
 * @Date 2019/10/9 16:32
 * @Version 1.0
 **/
@Controller
@Api(tags = "WcCompanyInfoController",description = "文创企业信息管理")
@RequestMapping("/wcCompany")
public class WcCompanyInfoController {

    @Autowired
    private WcCompanyInfoService wcCompanyInfoService;

    @ResponseBody
    @RequestMapping(value = "/listAll",method = RequestMethod.GET)
    @ApiOperation("查询所有文创企业信息")
    public CommonResult<List<WcCompanyInfo>> listAll(){

     List<WcCompanyInfo>  wcCompanyInfoList=wcCompanyInfoService.listAll();
        return CommonResult.success(wcCompanyInfoList);
    }
}
