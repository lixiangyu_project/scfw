package com.citycloud.sc.controller;

import com.citycloud.sc.common.api.CommonResult;
import com.citycloud.sc.model.DataDictionary;
import com.citycloud.sc.model.WcCompanyInfo;
import com.citycloud.sc.service.DataDictionaryService;
import com.citycloud.sc.service.WcCompanyInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName DataDictionaryController
 * @Description TODO
 * @Author 相信飞鱼
 * @Date 2019/10/11 15:41
 * @Version 1.0
 **/
@Controller
@Api(tags = "DataDictionaryController",description = "数据字典管理")
@RequestMapping("/dataDictionary")
public class DataDictionaryController {
    @Autowired
    private DataDictionaryService dataDictionaryService;

    @ApiOperation(value = "根据目录查询字典(restful风格)")
    @RequestMapping(value = "/{content}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<List<DataDictionary>> getData(@PathVariable("content") String content) {
        return CommonResult.success(dataDictionaryService.getDataDictionary(content));
    }

    @ApiOperation(value = "根据目录查询字典")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<List<DataDictionary>> list(@RequestParam(value = "content", required = false) String content) {
        return CommonResult.success(dataDictionaryService.getDataDictionary(content));
    }


}
