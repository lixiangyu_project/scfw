package com.citycloud.sc.controller;

import com.citycloud.sc.common.api.CommonPage;
import com.citycloud.sc.common.api.CommonResult;
import com.citycloud.sc.dto.WcStreetInfoParam;
import com.citycloud.sc.dto.WcStreetInfoResult;
import com.citycloud.sc.model.WcCompanyInfo;
import com.citycloud.sc.model.WcCompanyinfoCount;
import com.citycloud.sc.model.WcIndustryinfoCount;
import com.citycloud.sc.model.WcStreetinfoCount;
import com.citycloud.sc.service.WcCompanyInfoService;
import com.citycloud.sc.service.WcInfoShowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName WcInfoShowController
 * @Description 文创信息展示
 * @Author 相信飞鱼
 * @Date 2019/10/14 17:00
 * @Version 1.0
 **/
@Controller
@Api(tags = "WcInfoShowController",description = "文创信息展示")
@RequestMapping("/wcInfoShow")
public class WcInfoShowController {
    @Autowired
    private WcInfoShowService wcInfoShowService;

    @ApiOperation("统计文创信息")
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult count() {
        try {
            wcInfoShowService.count();
        } catch (Exception e) {
            e.printStackTrace();
            return CommonResult.failed("统计失败");
        }
        return CommonResult.success("统计成功");
    }

    @ResponseBody
    @RequestMapping(value = "/getIndustryInfo",method = RequestMethod.GET)
    @ApiOperation("查询行业信息")
    public CommonResult<List<WcIndustryinfoCount>> getIndustryInfo(@RequestParam (value = "year", required = false) String year){

        List<WcIndustryinfoCount>  wcIndustryinfoCountList=wcInfoShowService.getIndustryInfo(year);
        return CommonResult.success(wcIndustryinfoCountList);
    }

    @ApiOperation("分页查询文创企业信息")
    @RequestMapping(value = "/listCompanyInfo", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<CommonPage<WcCompanyinfoCount>> listCompanyInfo(@RequestParam(value = "name", required = false) String name,
                                                                        @RequestParam(value = "year", required = false) String year,
                                                             @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                                             @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        List<WcCompanyinfoCount> wcCompanyinfoCountList = wcInfoShowService.listCompanyInfo(name,year,pageSize, pageNum);
        return CommonResult.success(CommonPage.restPage(wcCompanyinfoCountList));
    }

    @ResponseBody
    @RequestMapping(value = "/getStreetInfo",method = RequestMethod.GET)
    @ApiOperation("查询街道文创信息")
    public CommonResult<List<WcStreetinfoCount>> getStreetInfo(@RequestParam (value = "year", required = false) String year){

        List<WcStreetinfoCount>  wcStreetinfoCounts=wcInfoShowService.getStreetInfo(year);
        return CommonResult.success(wcStreetinfoCounts);
    }

    @ResponseBody
    @RequestMapping(value = "/countStreetInfo",method = RequestMethod.POST)
    @ApiOperation("统计街道文创信息")
    public CommonResult<List<WcStreetInfoResult>> countStreetInfo(@Validated @RequestBody WcStreetInfoParam wcStreetInfoParam){

        List<WcStreetInfoResult>  wcStreetInfoResults=wcInfoShowService.countStreetInfo(wcStreetInfoParam);
        return CommonResult.success(wcStreetInfoResults);
    }
}
