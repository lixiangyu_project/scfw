package com.citycloud.sc.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * @ClassName WcProjectInfoController
 * @Description TODO
 * @Author 相信飞鱼
 * @Date 2019/10/10 16:34
 * @Version 1.0
 **/
@Controller
@Api(tags = "WcProjectInfoController",description = "文创项目管理")
@RequestMapping("/wcProject")
public class WcProjectInfoController {


    @ResponseBody
    @RequestMapping(value = "/download",method = RequestMethod.GET)
    @ApiOperation("下载文创项目")
    public String downloadFile(String fileName, HttpServletRequest request, HttpServletResponse response) {
        if (fileName != null) {
            //设置文件路径
            String realPath = "D:/usr/local/tomcat7/webapps/snow6/file/";
            File file = new File(realPath , fileName);
            if (file.exists()) {
                response.setContentType("application/force-download");// 设置强制下载不打开
                response.addHeader("Content-Disposition", "attachment;fileName=" + fileName);// 设置文件名
                byte[] buffer = new byte[1024];
                FileInputStream fis = null;
                BufferedInputStream bis = null;
                try {
                    fis = new FileInputStream(file);
                    bis = new BufferedInputStream(fis);
                    OutputStream os = response.getOutputStream();
                    int i = bis.read(buffer);
                    while (i != -1) {
                        os.write(buffer, 0, i);
                        i = bis.read(buffer);
                    }
                    System.out.println("success");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (bis != null) {
                        try {
                            bis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (fis != null) {
                        try {
                            fis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return null;
    }
}
