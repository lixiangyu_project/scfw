package com.citycloud.sc.service.impl;

import com.citycloud.sc.mapper.WcCompanyInfoMapper;
import com.citycloud.sc.model.WcCompanyInfo;
import com.citycloud.sc.model.WcCompanyInfoExample;
import com.citycloud.sc.service.WcCompanyInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName WcCompanyInfoServiceImpl
 * @Description WcCompanyInfoService实现类
 * @Author lixy
 * @Date 2019/10/9 16:40
 * @Version 1.0
 **/
@Service
public class WcCompanyInfoServiceImpl  implements WcCompanyInfoService {

    @Autowired
    private WcCompanyInfoMapper wcCompanyInfoMapper;
    @Override
    public List<WcCompanyInfo> listAll() {
        return wcCompanyInfoMapper.selectByExample(new WcCompanyInfoExample());
    }
}
