package com.citycloud.sc.service.impl;

import com.citycloud.sc.dao.WcInfoShowDao;
import com.citycloud.sc.dto.WcStreetInfoParam;
import com.citycloud.sc.dto.WcStreetInfoResult;
import com.citycloud.sc.mapper.*;
import com.citycloud.sc.model.*;
import com.citycloud.sc.service.WcInfoShowService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName WcInfoShowServiceImpl
 * @Description 文创信息展示实现层
 * @Author 相信飞鱼
 * @Date 2019/10/14 17:14
 * @Version 1.0
 **/
@Service
public class WcInfoShowServiceImpl implements WcInfoShowService {

    @Autowired
    private WcIndustryinfoCountMapper wcIndustryinfoCountMapper;
    @Autowired
    private WcCompanyinfoCountMapper wcCompanyinfoCountMapper;
    @Autowired
    private WcInfoShowDao wcInfoShowDao;
    @Autowired
    private WcCompanyCountMapper wcCompanyCountMapper;
    @Autowired
    private WcProjectInfoMapper wcProjectInfoMapper;
    @Autowired
    private WcCompanyTaxationMapper wcCompanyTaxationMapper;
    @Autowired
    private WcStreetinfoCountMapper wcStreetinfoCountMapper;
    @Autowired
    private WcCompanyInfoMapper wcCompanyInfoMapper;

    @Override
    public void count() {
        //1,先清除行业统计表,企业统计表和街道信息统计表
        wcIndustryinfoCountMapper.deleteByExample(new WcIndustryinfoCountExample());
        wcCompanyinfoCountMapper.deleteByExample(new WcCompanyinfoCountExample());
        wcStreetinfoCountMapper.deleteByExample(new WcStreetinfoCountExample());

        //2,统计行业信息
        List<WcIndustryinfoCount> wcIndustryinfoCountList = wcInfoShowDao.getInfoByIndustry();
        if (wcIndustryinfoCountList != null && !wcIndustryinfoCountList.isEmpty()) {
            for (WcIndustryinfoCount wcIndustryinfoCount :
                    wcIndustryinfoCountList) {
                wcIndustryinfoCountMapper.insert(wcIndustryinfoCount);
            }
        }
        //3,统计企业信息
        List<WcCompanyinfoCount> wcCompanyinfoCountList = new ArrayList<>();
        //查询企业统计信息
        List<WcCompanyCount> wcCompanyCounts = wcCompanyCountMapper.selectByExample(new WcCompanyCountExample());
        if (wcCompanyCounts != null && !wcCompanyCounts.isEmpty()) {
            for (WcCompanyCount wcCompanyCount : wcCompanyCounts
            ) {
                WcCompanyinfoCount wcCompanyinfoCount = new WcCompanyinfoCount();
                String name = wcCompanyCount.getName();
                wcCompanyinfoCount.setYear(wcCompanyCount.getYear());
                wcCompanyinfoCount.setStreet(wcCompanyCount.getStreet());
                wcCompanyinfoCount.setName(name);
                wcCompanyinfoCount.setCreditCode(wcCompanyCount.getCreditCode());
                wcCompanyinfoCount.setIncreaseValue(wcCompanyCount.getIncreaseValue());
                wcCompanyinfoCount.setPeriod(wcCompanyCount.getPeriod());
                wcCompanyinfoCount.setIncreaseSpeed(wcCompanyCount.getIncreaseSpeed());
                getProjectInfo(wcCompanyinfoCount, name);
                wcCompanyinfoCountList.add(wcCompanyinfoCount);
            }
        }
        //查询企业税收信息
        List<WcCompanyTaxation> wcCompanyTaxations = wcCompanyTaxationMapper.selectByExample(new WcCompanyTaxationExample());
        if (wcCompanyTaxations != null && !wcCompanyTaxations.isEmpty()) {
            for (WcCompanyTaxation wcCompanyTaxation :
                    wcCompanyTaxations) {
                WcCompanyinfoCount wcCompanyinfoCount = new WcCompanyinfoCount();
                wcCompanyinfoCount.setYear(wcCompanyTaxation.getYear());
                wcCompanyinfoCount.setStreet(wcCompanyTaxation.getStreet());
                wcCompanyinfoCount.setName(wcCompanyTaxation.getName());
                wcCompanyinfoCount.setCreditCode(wcCompanyTaxation.getCreditCode());
                String increaseValue = wcCompanyTaxation.getTaxIncome();
                wcCompanyinfoCount.setTaxIncome(increaseValue);
                String period = wcCompanyTaxation.getPeriod();
                wcCompanyinfoCount.setPeriod(period);
                DecimalFormat df = new DecimalFormat("0.00");
                String increaseSpeed = df.format((Double.parseDouble(increaseValue) - Double.parseDouble(period)) / Double.parseDouble(period));
                wcCompanyinfoCount.setIncreaseSpeed(increaseSpeed);
                getProjectInfo(wcCompanyinfoCount, wcCompanyTaxation.getName());
                wcCompanyinfoCountList.add(wcCompanyinfoCount);
            }
        }
        //添入企业统计信息表
        if (wcCompanyinfoCountList != null && !wcCompanyinfoCountList.isEmpty()) {
            for (WcCompanyinfoCount wcCompanyinfoCount :
                    wcCompanyinfoCountList) {
                wcCompanyinfoCountMapper.insert(wcCompanyinfoCount);
            }
        }
        //3,街道信息统计
        //查询街道的统计和税收数据
        List<WcStreetinfoCount> countAndTaxCounts = wcInfoShowDao.getCountAndTaxByStreet();
        //添加到街道统计信息表里
        if(countAndTaxCounts!=null&&!countAndTaxCounts.isEmpty()){
            for (WcStreetinfoCount wcStreetinfoCount: countAndTaxCounts) {
                wcStreetinfoCount.setSupportMonenynum(new BigDecimal(0));
                wcStreetinfoCount.setSupportCompanynum(0);
                wcStreetinfoCount.setSupportProjectnum(0);
                if(wcStreetinfoCount.getType().equals("2")){
                    wcStreetinfoCount.setTaxIncome(wcStreetinfoCount.getIncreaseValue());
                    wcStreetinfoCount.setIncreaseValue(new BigDecimal(0));
                }
                if(wcStreetinfoCount.getType().equals("1")){
                    wcStreetinfoCount.setTaxIncome(new BigDecimal(0));
                }
                wcStreetinfoCountMapper.insertSelective(wcStreetinfoCount);
            }
        }
       //根据年度,类型和街道补充项目数据到街道统计信息表
        List<WcStreetinfoCount> wcStreetinfoCounts = new ArrayList<>();
        //查询项目信息
        List<WcProjectInfo> wcProjectInfos = wcProjectInfoMapper.selectByExample(new WcProjectInfoExample());
        if (wcProjectInfos != null && !wcProjectInfos.isEmpty()) {
            for (WcProjectInfo wcProjectInfo : wcProjectInfos) {
                //得到企业信息
                String name = wcProjectInfo.getSupportCompany();
                BigDecimal supportAmount = wcProjectInfo.getSupportAmount();
                String year = wcProjectInfo.getYear();
                String type="1";
                //查询该企业所在的街道
                WcCompanyInfoExample wcCompanyInfoExample = new WcCompanyInfoExample();
                wcCompanyInfoExample.createCriteria().andNameEqualTo(name);
                List<WcCompanyInfo> wcCompanyInfoList = wcCompanyInfoMapper.selectByExample(wcCompanyInfoExample);
                if (wcCompanyInfoList != null && !wcCompanyInfoList.isEmpty()) {
                    WcCompanyInfo wcCompanyInfo = wcCompanyInfoList.get(0);
                    String street = wcCompanyInfo.getStreet();
                    //根据年度和街道修改街道统计信息
                    WcStreetinfoCountExample wcStreetinfoCountExample=new WcStreetinfoCountExample();
                    WcStreetinfoCountExample.Criteria criteria=wcStreetinfoCountExample.createCriteria();
                    if(!StringUtils.isEmpty(year)){
                        criteria.andYearEqualTo(year);
                    }
                    if(!StringUtils.isEmpty(street)){
                        criteria.andStreetEqualTo(street);
                    }
                    if(!StringUtils.isEmpty(type)){
                        criteria.andTypeEqualTo(type);
                    }
                    //查询上一次统计数据
                    List<WcStreetinfoCount> oldWcStreetinfoCounts= wcStreetinfoCountMapper.selectByExample(wcStreetinfoCountExample);
                    WcStreetinfoCount oldWcStreetinfoCount=new WcStreetinfoCount();
                    if(oldWcStreetinfoCounts!=null&!oldWcStreetinfoCounts.isEmpty()){
                        oldWcStreetinfoCount= oldWcStreetinfoCounts.get(0);
                    }
                    int oldSupportCompanyNum = oldWcStreetinfoCount.getSupportCompanynum();
                    int oldSupportProjectNum = oldWcStreetinfoCount.getSupportProjectnum();
                    BigDecimal oldSupportMoneyNum = oldWcStreetinfoCount.getSupportMonenynum();
                    //统计当前数据
                    int supportProjectNum=oldSupportCompanyNum+1;
                    int supportCompanyNum=oldSupportProjectNum+1;
                    BigDecimal supportMoneyNum=oldSupportMoneyNum.add(supportAmount);
                    WcStreetinfoCount wcStreetinfoCount=new WcStreetinfoCount();
                    wcStreetinfoCount.setSupportProjectnum(supportProjectNum);
                    wcStreetinfoCount.setSupportCompanynum(supportCompanyNum);
                    wcStreetinfoCount.setSupportMonenynum(supportMoneyNum);
                    wcStreetinfoCountMapper.updateByExampleSelective(wcStreetinfoCount,wcStreetinfoCountExample);
                }
            }

        }
    }

    @Override
    public List<WcIndustryinfoCount> getIndustryInfo(String year) {
        WcIndustryinfoCountExample wcIndustryinfoCountExample = new WcIndustryinfoCountExample();
        if (!StringUtils.isEmpty(year)) {
            wcIndustryinfoCountExample.createCriteria().andYearEqualTo(year);
        }
        return wcIndustryinfoCountMapper.selectByExample(wcIndustryinfoCountExample);
    }

    @Override
    public List<WcCompanyinfoCount> listCompanyInfo(String name, String year, Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        WcCompanyinfoCountExample wcCompanyinfoCountExample = new WcCompanyinfoCountExample();
        WcCompanyinfoCountExample.Criteria criteria = wcCompanyinfoCountExample.createCriteria();
        if (!StringUtils.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        if (!StringUtils.isEmpty(year)) {
            criteria.andYearEqualTo(year);
        }
        return wcCompanyinfoCountMapper.selectByExample(wcCompanyinfoCountExample);
    }

    @Override
    public List<WcStreetinfoCount> getStreetInfo(String year) {
        WcStreetinfoCountExample wcStreetinfoCountExample = new WcStreetinfoCountExample();
        if (!StringUtils.isEmpty(year)) {
            wcStreetinfoCountExample.createCriteria().andYearEqualTo(year);
        }
        return wcStreetinfoCountMapper.selectByExample(wcStreetinfoCountExample);
    }

    @Override
    public List<WcStreetInfoResult> countStreetInfo(WcStreetInfoParam wcStreetInfoParam) {
        return  wcInfoShowDao.countStreetInfo(wcStreetInfoParam);
    }

    public void getProjectInfo(WcCompanyinfoCount wcCompanyinfoCount, String name) {
        // 获取扶持该企业的项目信息
        WcProjectInfoExample wcProjectInfoExample = new WcProjectInfoExample();
        wcProjectInfoExample.createCriteria().andSupportCompanyEqualTo(name);
        List<WcProjectInfo> wcProjectInfos = wcProjectInfoMapper.selectByExample(wcProjectInfoExample);
        //获取项目数
        wcCompanyinfoCount.setSupportProjectnum(wcProjectInfos.size());
        //获取扶持金额
        BigDecimal support_moneyNum = new BigDecimal(0);
        BigDecimal support_amount;
        if (wcProjectInfos != null && !wcProjectInfos.isEmpty()) {
            for (WcProjectInfo wcProjectInfo :
                    wcProjectInfos) {
                support_amount = wcProjectInfo.getSupportAmount();
                support_moneyNum = support_moneyNum.add(support_amount);
            }
            wcCompanyinfoCount.setSupportMonenynum(support_moneyNum + "");
        } else {
            wcCompanyinfoCount.setSupportMonenynum("0.00");
        }
    }
}
