package com.citycloud.sc.service;

import com.citycloud.sc.dto.WcStreetInfoParam;
import com.citycloud.sc.dto.WcStreetInfoResult;
import com.citycloud.sc.model.WcCompanyinfoCount;
import com.citycloud.sc.model.WcIndustryinfoCount;
import com.citycloud.sc.model.WcStreetinfoCount;

import java.util.List;

public interface WcInfoShowService {

    /**
     * 统计文创信息
     */
    void count();

    /**
     * 查询行业统计信息
     */
    List<WcIndustryinfoCount> getIndustryInfo(String year);

    /**
     * 分页查询企业统计信息
     */
    List<WcCompanyinfoCount> listCompanyInfo(String name, String year,Integer pageSize, Integer pageNum);
    /**
     * 查询街道统计信息
     */
    List<WcStreetinfoCount> getStreetInfo(String year);

    /**
     * 统计街道信息
     */
    List<WcStreetInfoResult> countStreetInfo(WcStreetInfoParam wcStreetInfoParam);
}
