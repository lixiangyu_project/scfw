package com.citycloud.sc.service.impl;

import com.citycloud.sc.mapper.DataContentsMapper;
import com.citycloud.sc.mapper.DataDictionaryMapper;
import com.citycloud.sc.model.DataContents;
import com.citycloud.sc.model.DataContentsExample;
import com.citycloud.sc.model.DataDictionary;
import com.citycloud.sc.model.DataDictionaryExample;
import com.citycloud.sc.service.DataDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName DataDictionaryServiceImpl
 * @Description 字典管理实现类
 * @Author 相信飞鱼
 * @Date 2019/10/11 15:58
 * @Version 1.0
 **/
@Service
public class DataDictionaryServiceImpl implements DataDictionaryService {

    @Autowired
    private DataDictionaryMapper dataDictionaryMapper;
    @Autowired
    private DataContentsMapper dataContentsMapper;

    @Override
    public List<DataDictionary> getDataDictionary(String content) {

       //先去目录表找到目录ID
        DataContentsExample dataContentsExample=new DataContentsExample();
        dataContentsExample.createCriteria().andNameEqualTo(content);
        List<DataContents> dataContents=dataContentsMapper.selectByExample(dataContentsExample);
       String contentId=dataContents.get(0).getId();
       //根据目录Id找到字典数据
        DataDictionaryExample dataDictionaryExample =new DataDictionaryExample();
        dataDictionaryExample.createCriteria().andContentIdEqualTo(contentId);
        return  dataDictionaryMapper.selectByExample(dataDictionaryExample);
    }
}
