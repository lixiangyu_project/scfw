package com.citycloud.sc.service;

import com.citycloud.sc.model.WcCompanyInfo;

import java.util.List;

public interface WcCompanyInfoService {
    /**
     * description: 查询所有文创企业信息
     * @params  * @Param: null
     * @return
     */
    List<WcCompanyInfo> listAll();
}
