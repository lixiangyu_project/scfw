package com.citycloud.sc.service;

import com.citycloud.sc.model.DataDictionary;

import java.util.List;

public interface DataDictionaryService {

    List<DataDictionary> getDataDictionary(String content);
}
