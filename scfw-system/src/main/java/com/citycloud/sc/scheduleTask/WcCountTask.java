package com.citycloud.sc.scheduleTask;

import com.citycloud.sc.dao.WcInfoShowDao;
import com.citycloud.sc.mapper.*;
import com.citycloud.sc.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName WcCountTask
 * @Description 文创信息综合统计
 * @Author 相信飞鱼
 * @Date 2019/10/14 16:37
 * @Version 1.0
 **/
@Component
public class WcCountTask {



    private static final Logger log = LoggerFactory.getLogger(WcCountTask.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    //@Scheduled(cron = "0/5 * * * * ?")
    public void reportCurrentTime() {
        log.info("The time is now {}", dateFormat.format(new Date()));
    }

    /**
     * description: 文创信息统计
     * @return
     * @params * @Param: null
     */
   // @Scheduled(cron = "0 10 18 * * ?")
    public void countWcInfo() {

    }

}