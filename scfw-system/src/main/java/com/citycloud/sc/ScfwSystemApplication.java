package com.citycloud.sc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * create by: lixy
 * description: 应用启动入口
 * create time: 2019/10/9 14:52
 */
@SpringBootApplication
@EnableScheduling
public class ScfwSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScfwSystemApplication.class, args);
    }

}
